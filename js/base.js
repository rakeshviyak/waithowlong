Parse.initialize("3btGHFCtTBOHdBfHSTNBfQSxqKTdOhvtUC8xaZcB", "d4xa0pZD4xSv6YLiJod5nyKx1MtoVj7MKEOS1nLz");

var EventObject = Parse.Object.extend("Event");
var eventQ = new Parse.Query(EventObject);
eventQ.get("FKmd9I3hHU", {
  success: function(eventq) {
    // The object was retrieved successfully.
    console.log(eventq.attributes.name);
    var duration = eventq.attributes.duration;
    var d = document.createElement("div"); 
    d.setAttribute('id','durationTime');
    d.setAttribute('value',duration);
    document.body.appendChild(d);
    var e =document.getElementById("ptime").innerHTML;
    if(e){
        document.getElementById("ptime").innerHTML=duration.toString().toHHMM();
        document.getElementById("ntime").innerHTML=duration.toString().toHHMM();
    }

  },
  error: function(object, error) {
            console.log("error");
  }
});

//ewma function
function ewMA(currentValue){
    var duration=parseInt(document.getElementById("durationTime").getAttribute('value'));
    var newValue = Math.round(0.1*currentValue+0.9*duration);
    var EventObject = Parse.Object.extend("Event");
    var eventQ = new Parse.Query(EventObject);
    eventQ.get("FKmd9I3hHU", {
      success: function(eventq) {
        console.log(duration+','+currentValue+','+newValue);
        eventq.set("duration", newValue);
        eventq.save();
        clearTimeout(myVarTimer);
        document.getElementById('timer-elements').innerHTML="Thank you. Have a great day.";
      },
      error: function(object,error){

      }
    })
    console.log(newValue);
}

//seconds to hours and minutes
String.prototype.toHHMMSS = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    var time    = hours+':'+minutes+":"+seconds;
    return time;
}


//seconds to hours and minutes
String.prototype.toHHMM = function () {
    var sec_num = parseInt(this, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);


    var m = (((minutes + 7.5)/15 | 0) * 15) % 60;
    var h = ((((minutes/105) + .5) | 0) + hours) % 24;
    // if (hours   < 10) {hours   = ""+hours;}
    // if (minutes < 10) {minutes = ""+minutes;}
    // if (seconds < 10) {seconds = ""+seconds;}
    if (!m){
        var time    = h+' hours ';
    }else if(!h){
        var time    = m+' minutes';
    }else{
        var time    = h+' hours ' + m +' minutes';
    }
    return time;
}


//get query string
function getParameterByName(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function setStartTime(){
    var contact = parseInt(document.getElementById('hpno').value);
    var startTime = new Date();
    var EventObject = Parse.Object.extend("Event");
    var eventQ = new Parse.Query(EventObject);
    eventQ.get("FKmd9I3hHU", {
      success: function(eventq) {
        var QueueObject = Parse.Object.extend("Queue");
        var queueObject = new QueueObject();
        queueObject.set("event",eventq);
        queueObject.set("HpNo",contact);
        queueObject.set("startTime",startTime);
        queueObject.set("estimateEnd",new Date(startTime.getTime()+(eventq.attributes.duration*1000)));
        queueObject.save(null, {
          success: function(object) {
            console.log("success write");
            var rlink="contact.html?hp="+contact;
            $.get('sendSMS.php?mobile='+contact+'&event='+1);

            window.location.href = rlink;
          },
          error: function(model, error) {
            console.log("fail write");
          }
        });
      },
      error: function(object,error){
            console.log("cant find the event object");
      }
    })
}

function setEndTime(contact,endTime){
    var EventObject = Parse.Object.extend("Event");
    var eventQ = new Parse.Query(EventObject);
    eventQ.get("FKmd9I3hHU", {
      success: function(eventq) {
        var QueueObject = Parse.Object.extend("Queue");
        var query = new Parse.Query(QueueObject);
        query.equalTo("HpNo", contact);
        query.find({
          success: function(results) {
            console.log("Successfully retrieved " + results.length + " scores.");
            // Do something with the returned Parse.Object values
            for (var i = 0; i < results.length; i++) { 
              var object = results[i];
              console.log(object.id + ' - ' + object.get('HpNo'));
              object.set("actualEnd",endTime);
              object.save(); 
              ewMA((endTime.getTime()-object.get('startTime').getTime())/1000);
            }
          },
          error: function(error) {
            alert("Error: " + error.code + " " + error.message);
          }
        });
        }
    });
}

function queryTimer(){
    var hpNo=getParameterByName('hp');
    console.log(hpNo);
    if (hpNo){
        var EventObject = Parse.Object.extend("Event");
        var eventQ = new Parse.Query(EventObject);
        eventQ.get("FKmd9I3hHU", {
          success: function(eventq) {
            var QueueObject = Parse.Object.extend("Queue");
            var query = new Parse.Query(QueueObject);
            query.equalTo("HpNo", parseInt(hpNo));
            query.find({
              success: function(results) {
                console.log(results);
                console.log("Successfully retrieved " + results.length + " scores.");
                // Do something with the returned Parse.Object values
                for (var i = 0; i < results.length; i++) { 
                  var object = results[i];
                  // console.log(object.id + ' - ' + object.get('startTime'));
                  var diffTime=(object.get('estimateEnd').getTime()-object.get('startTime').getTime())/1000;

                  document.getElementById('estimateTime').innerHTML=diffTime.toString().toHHMM();
                  date_time('date_time',object.get('startTime').getTime());
                }
              },
              error: function(error) {
                alert("Error: " + error.code + " " + error.message);
              }
            });
            }
        });
    }
}

function waitDuration(contact,waitTime){
    var EventObject = Parse.Object.extend("Event");
    var eventQ = new Parse.Query(EventObject);
    eventQ.get("FKmd9I3hHU", {
      success: function(eventq) {
        var QueueObject = Parse.Object.extend("Queue");
        var queueObject = new QueueObject();
        queueObject.set("event",eventq);
        queueObject.set("HpNo",contact);
        d=new Date();
        queueObject.set("startTime",new Date(d.getTime()+(waitTime*60*1000)));
        queueObject.set("actualEnd",new Date());
        queueObject.save(null, {
          success: function(object) {
            console.log("success write");
          },
          error: function(model, error) {
            console.log("fail write");
          }
        });
      },
      error: function(object,error){
            console.log("cant find the event object");
      }
    });
}
var myVarTimer;
function date_time(id,input_time)
{       
        var inputTime=input_time;
        var currentTime = new Date();
        diffTime=((currentTime.getTime() - inputTime)/1000);
        var d=diffTime.toString().toHHMMSS();
        document.getElementById(id).innerHTML = d;
        myVarTimer=setTimeout('date_time("'+id+'",'+inputTime+');','1000');
        return true;
}


