  window.onload = function () {
    var chart = new CanvasJS.Chart("chartContainer",
    {      
      theme:"theme2",
      title:{
        text: "Wait How Long??"
      },
      animationEnabled: true,
      axisY :{
        includeZero: false,
        // suffix: " k",
        // valueFormatString: "#,,.",
        suffix: " hr"
        
      },
      toolTip: {
        shared: "true"
      },
      data: [
      // {        
      //   type: "spline", 
      //   showInLegend: true,
      //   name: "Season 2",
      //   // markerSize: 0,        
      //   // color: "rgba(54,158,173,.6)",
      //   dataPoints: [
      //   {label: "12am", y: 10},
      //   {label: "3am", y: 3},        
      //   {label: "6am", y: 3.5},        
      //   {label: "9am", y: 6},        
      //   {label: "12pm", y: 8},        
      //   {label: "3pm", y: 9},        
      //   {label: "6pm", y: 12},        
      //   {label: "9pm", y: 14},             
      //   {label: "11pm", y: 12},            

      //   ]
      // },
      {        
        type: "spline", 
        showInLegend: true,
        // markerSize: 0,
        name: "Avg. waiting time",
        dataPoints: [
        {label: "12pm", y: 8},        
        {label: "3pm", y: 9},        
        {label: "6pm", y: 12},        
        {label: "9pm", y: 14},             
        {label: "12am", y: 10},
        {label: "3am", y: 3},        
        {label: "6am", y: 3.5},        
        {label: "9am", y: 6},        

        ]
      } 
      

      ],
      legend:{
        cursor:"pointer",
        itemclick : function(e) {
          if (typeof(e.dataSeries.visible) === "undefined" || e.dataSeries.visible ){
            e.dataSeries.visible = false;
          }
          else {
            e.dataSeries.visible = true;
          }
          chart.render();
        }
        
      },
    });
  chart.render();
  }
